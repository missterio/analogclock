//
//  ViewController.swift
//  ClockTest
//
//  Created by Nizar Hamouda on 26/02/2019.
//  Copyright © 2019 Nizar Hamouda. All rights reserved.
//

import UIKit


class ClockViewController: UIViewController, CAAnimationDelegate {
    
  @objc   func rotateLayer(currentLayer:CALayer,dur:CFTimeInterval){
        
        let angle = degree2radian(360)
        
        let theAnimation = CABasicAnimation(keyPath:"transform.rotation.z")
        theAnimation.duration = dur
        // Make this view controller the delegate so it knows when the animation starts and ends
        theAnimation.delegate = self
        theAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        // Use fromValue and toValue
        theAnimation.fromValue = 0
        theAnimation.repeatCount = Float.infinity
        theAnimation.toValue = angle
        
        
        
        // Add the animation to the layer
        currentLayer.add(theAnimation, forKey:"rotate")
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Setup
        let endAngle = CGFloat(2*Double.pi)
        
        
        let newView = ClockView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        
        
        self.view.addSubview(newView)
        let time = timeCoords(x: newView.frame.midX, y: newView.frame.midY, time: ctime(),radius: 50)
        // Do any additional setup after loading the view, typically from a nib.
        // Hours
        let hourLayer = CAShapeLayer()
        hourLayer.frame = newView.frame
        let path = CGMutablePath()
        
        path.move(to: CGPoint(x:newView.frame.midX, y:newView.frame.midY))
        path.addLine(to: CGPoint(x:time.h.x, y:time.h.y))
        hourLayer.path = path
        hourLayer.lineWidth = 4
        hourLayer.lineCap = CAShapeLayerLineCap.round
        hourLayer.strokeColor = UIColor.white.cgColor
        hourLayer.rasterizationScale = UIScreen.main.scale;
        hourLayer.shouldRasterize = true
        
        self.view.layer.addSublayer(hourLayer)
        // time it takes for hour hand to pass through 360 degress
        rotateLayer(currentLayer: hourLayer,dur:43200)
        
        
        // Minutes
        let minuteLayer = CAShapeLayer()
        minuteLayer.frame = newView.frame
        let minutePath = CGMutablePath()
        
        minutePath.move(to: CGPoint(x:newView.frame.midX, y:newView.frame.midY))
        minutePath.addLine(to: CGPoint(x:time.m.x, y:time.m.y))
        minuteLayer.path = minutePath
        minuteLayer.lineWidth = 3
        minuteLayer.lineCap = CAShapeLayerLineCap.round
        minuteLayer.strokeColor = UIColor.white.cgColor
        
        minuteLayer.rasterizationScale = UIScreen.main.scale;
        minuteLayer.shouldRasterize = true
        
        self.view.layer.addSublayer(minuteLayer)
        rotateLayer(currentLayer: minuteLayer,dur: 3600)
        
        // Seconds
        let secondLayer = CAShapeLayer()
        secondLayer.frame = newView.frame
        // glow effect
        secondLayer.shadowOpacity = 1
        secondLayer.shadowColor = UIColor.white.cgColor
        secondLayer.shadowOffset = CGSize.zero
        secondLayer.shadowRadius = 5
    
        
        let secondPath = CGMutablePath()
        secondPath.move(to: CGPoint(x:newView.frame.midX, y:newView.frame.midY))
        secondPath.addLine(to: CGPoint(x:time.s.x, y: time.s.y))
        
        
        secondLayer.path = secondPath
        secondLayer.lineWidth = 1.5
        secondLayer.lineCap = CAShapeLayerLineCap.round
        secondLayer.strokeColor =  UIColor(red: 255/255, green: 0/255, blue: 187/255, alpha: 1).cgColor
        secondLayer.rasterizationScale = UIScreen.main.scale;
        secondLayer.shouldRasterize = true
        
     
        
        //instance of CAReplicatorLayer
        let replicatorLayer = CAReplicatorLayer()
        replicatorLayer.frame = secondLayer.bounds
        // number of instances
        replicatorLayer.instanceCount = 25 // or 30
        // drawing delay
        replicatorLayer.instanceDelay = CFTimeInterval(1 / 30.0)
        // 2D
        replicatorLayer.preservesDepth = false
        replicatorLayer.instanceColor = UIColor.white.cgColor
        // red/green/blue offsets to the color values of each successive replicated instance
        replicatorLayer.instanceRedOffset = 240
        replicatorLayer.instanceGreenOffset = 0
        replicatorLayer.instanceBlueOffset = 170
        replicatorLayer.instanceAlphaOffset = 1.0
        // rotate each successive instance around a circle
        let angle = Float(Double.pi * 2.0) / 30
        replicatorLayer.instanceTransform = CATransform3DMakeRotation(CGFloat(angle), 0.0, 0.0, 1.0)
        secondLayer.addSublayer(replicatorLayer)
        let instanceLayer = CALayer()
        let layerWidth: CGFloat = 10.0
        let midX = secondLayer.bounds.midX - layerWidth / 2.0
        instanceLayer.frame = CGRect(x: midX, y: 0.0, width: layerWidth, height: layerWidth * 3.0)
        instanceLayer.backgroundColor = UIColor(red: 255/255, green: 0/255, blue: 187/255, alpha: 1).cgColor
        replicatorLayer.addSublayer(instanceLayer)
        instanceLayer.opacity = 1.0
        let fadeAnimation = CABasicAnimation(keyPath: "opacity")
        fadeAnimation.fromValue = 1.0
        fadeAnimation.toValue = 0.0
        fadeAnimation.duration = 1
        fadeAnimation.repeatCount = Float.greatestFiniteMagnitude
        instanceLayer.add(fadeAnimation, forKey: "FadeAnimation")

  
        self.view.layer.addSublayer(secondLayer)
        rotateLayer(currentLayer: secondLayer,dur: 60)
        let centerPiece = CAShapeLayer()
        
        let circle = UIBezierPath(arcCenter: CGPoint(x:newView.frame.midX,y:newView.frame.midY), radius: 4.5, startAngle: 0, endAngle: endAngle, clockwise: true)
 
        centerPiece.path = circle.cgPath
        centerPiece.fillColor = UIColor.white.cgColor
        self.view.layer.addSublayer(centerPiece)
        
        
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
        
    }
    

}







