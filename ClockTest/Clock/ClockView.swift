//
//  ClockView.swift
//  ClockTest
//
//  Created by Nizar Hamouda on 27/02/2019.
//  Copyright © 2019 Nizar Hamouda. All rights reserved.
//

import Foundation
import UIKit

class ClockView: UIView {
    
    
    override func draw(_ rect:CGRect)
        
    {
        // obtain context
        let ctx = UIGraphicsGetCurrentContext()
        
        // decide on radius (clock radius)
        let rad = rect.width/3.5
        
        
        
        
        secondMarkers(ctx: ctx!, x: rect.midX, y: rect.midY, radius: rad, sides: 60, color: UIColor.white)
        
        drawText(rect:rect, ctx: ctx!, x: rect.midX, y: rect.midY, radius: rad, sides: .four, color: UIColor.green)
        
    }
}

enum NumberOfNumerals:Int {
    case two = 2, four = 4, twelve = 12
}
